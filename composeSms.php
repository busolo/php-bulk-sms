<?php
// Include necessary scripts and classes for entire project
include("includes/loader.php");
include("header.php");
@$id = $_GET["id"];
if($id) {
    //Get Campaign ID
    $arrProvider = find_recordFilter("tbl_campaigns", "campaign_id", $id, "campaign_id");
    ?>
    <div class="container">
        <div class="row">
            <section id="main-content">
                <section class="wrapper">
                    <div class="row">
                        <div class="col-sm-6">
                            <section>
                                <header class="panel-heading wht-bg">
                                    <h4 class="gen-case"> Compose SMS</h4>
                                </header>
                                <div class="panel-body">
                                    <div class="compose-mail">
                                        <form id="sendSmsForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                                            <label style="margin: 10px 5px; font-weight: 500;">Campaign Name e.g INEOS
                                                Reminder</label>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <input type="text" tabindex="1" id="name" class="form-control"
                                                           name="name" readonly value="<?= $arrProvider['name'] ?>">
                                                    <input type="hidden" tabindex="1" id="campaign_id" class="form-control"
                                                           name="campaign_id" readonly value="<?= $id ?>">
                                                    <input type="hidden" tabindex="1" id="type" class="form-control"
                                                           name="type" value="sms">
                                                </div>
                                            </div>
                                            <label style="margin: 10px 5px; font-weight: 500;">Enter Recipents seperated
                                                by comma e.g 0722xxxxxx,0733xxxxxx</label>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <input type="text" tabindex="1" id="phone" class="form-control"
                                                           name="phone" required>
                                                </div>
                                            </div>
                                            <label style="margin: 10px 5px 10px; font-weight: 500;">Enter
                                                Message</label>
                                            <div class="compose-editor">
                                                <textarea class="wysihtml5 form-control" rows="6" id="message"
                                                          name="message_area" required></textarea>
                                            </div>
                                            <div class="compose-btn pull-right" style="margin:20px 0 !important">
                                                <button type="submit" class="btn btn-primary"> Send </button>
                                                <button class="btn btn-sm"><i class="fa fa-times"></i> Discard</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="col-sm-3">
                            <section>
                                <header class="panel-heading wht-bg">
                                    <h4 class="gen-case text-center"> Preview </h4>
                                </header>
                                <div class="panel-body">
                                    <div class="phone-preview">
                                        <div id="presview">
                                            <textarea class="preview" id="preview" disabled></textarea>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <!-- page end-->
                </section>
            </section>
        </div>
    </div>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
    <script>
        // Bind to the submit event of our form
        $("#sendSmsForm").submit(function(e){
            // Prevent default posting of form
            e.preventDefault();
            var $form = $(this);
            // Serialize the data in the form
            var serializedData = $form.serialize();
            var phone = $("#phone").val();
            var type = $("#type").val();
            $.ajax({
                url: "addSMS.php",
                type: "post",
                data: {"campaign_id":$("#campaign_id").val(), "phone":$("#phone").val(), "type": $("#type").val(), "message": $("#message").val() },
                success: function(response){
                    if(!alert(response)){
                        window.location.replace("index.php");
                    }
                }
            });
        });
    </script>
    <?php
    include("footer.php");
} else {

}
?>

