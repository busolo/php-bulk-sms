<?php

function mysql_prep($string){
    global $connection;
    $escaped_string = mysqli_real_escape_string($connection, $string);
    return $escaped_string;
}

function confirm_query($result_set){
    if(!$result_set){
        //print_r(mysqli_error()); die();
        die("Database query failed.");
    }
}

function find_recordFilter($tbl, $where, $id, $order_by) {
    global $connection;
    $safe_tbl = mysqli_real_escape_string($connection, $tbl);
    $safe_where = mysqli_real_escape_string($connection, $where);
    $safe_id = mysqli_real_escape_string($connection, $id);
    $safe_order_by = mysqli_real_escape_string($connection, $order_by);
    $query = "SELECT *
		          FROM ".$safe_tbl."
		          WHERE ".$safe_where." = '".$safe_id."'
		          ORDER BY ".$safe_order_by." ASC
                    LIMIT 1";
    $record_set = mysqli_query($connection, $query);
    confirm_query($record_set);
    if($record = mysqli_fetch_assoc($record_set)){
        return $record;
    } else {
        return null;
    }
}

//Retrieve Pending SMS
function pendingSMS() {
    global $connection;
    $query = "SELECT tbl_campaigns.name as campaign, tbl_sms.phone_number, tbl_messages.message, tbl_sms.sms_id, 
                    tbl_messages.datetime, status FROM tbl_sms
                    LEFT JOIN tbl_messages ON tbl_messages.message_id = tbl_sms.message_id
                    LEFT JOIN tbl_campaigns ON tbl_campaigns.campaign_id = tbl_messages.campaign_id
                    ORDER BY sms_id ASC";
    $list_set = mysqli_query($connection, $query);
    confirm_query($list_set);
    return $list_set;
}

function getQueue() {
    global $connection;
    $query = "SELECT tbl_sms.phone_number, tbl_messages.message, tbl_sms.sms_id, tbl_messages.datetime FROM tbl_sms
                    LEFT JOIN tbl_messages ON tbl_messages.message_id = tbl_sms.message_id
                    WHERE status = 'PENDING'
                    ORDER BY sms_id ASC";
    $list_set = mysqli_query($connection, $query);
    confirm_query($list_set);
    return $list_set;
}

// Retrieve Lists
function retrieve_consolidated_list($list_name, $order_by){
    global $connection;
    $query = "SELECT * ";
    $query .= "FROM ". $list_name;
    $query .= " ORDER BY " . $order_by . " ASC";
    $list_set = mysqli_query($connection, $query);
    confirm_query($list_set);
    return $list_set;
}

function retrieve_list($list_name, $order_by){
    global $connection;
    $query = "SELECT * ";
    $query .= "FROM ". $list_name;
    $query .= " ORDER BY " . $order_by . " ASC";
    $list_set = mysqli_query($connection, $query);
    confirm_query($list_set);
    return $list_set;
}

function retrieve_list_desc($list_name, $order_by){
    global $connection;
    $query = "SELECT * ";
    $query .= "FROM ". $list_name;
    $query .= " ORDER BY " . $order_by . " DESC";
    $list_set = mysqli_query($connection, $query);
    confirm_query($list_set);
    return $list_set;
}

function find_records_by_filter_desc($record, $filter, $filter_value, $order_by){
    global $connection;
    $safe_record = mysqli_real_escape_string($connection, $record);
    $safe_filter = mysqli_real_escape_string($connection, $filter);
    $safe_filter_value = mysqli_real_escape_string($connection, $filter_value);
    $safe_order_by = mysqli_real_escape_string($connection, $order_by);
    $query = "SELECT * ";
    $query .= "FROM ". $safe_record;
    $query .= " WHERE ". $safe_filter;
    $query .= " = '{$safe_filter_value}' ";
    $query .= " ORDER BY " . $order_by . " DESC";
    $list_set = mysqli_query($connection, $query);
    confirm_query($list_set);
    return $list_set;
}

// Get record by id.
function find_record_by_id($record, $id, $id_value){
    global $connection;
    $safe_record = mysqli_real_escape_string($connection, $record);
    $safe_id = mysqli_real_escape_string($connection, $id);
    $safe_id_value = mysqli_real_escape_string($connection, $id_value);
    $query = "SELECT * ";
    $query .= "FROM ".$safe_record;
    $query .= " WHERE ". $safe_id;
    $query .= " = '{$safe_id_value}' ";
    $query .= "LIMIT 1";
    $record_set = mysqli_query($connection, $query);
    confirm_query($record_set);
    if($record = mysqli_fetch_assoc($record_set)){
        return $record;
    } else {
        return null;
    }
}

function find_consolidated_record_by_id($record, $id, $id_value){
    global $connection;
    $safe_record = mysqli_real_escape_string($connection, $record);
    $safe_id = mysqli_real_escape_string($connection, $id);
    $safe_id_value = mysqli_real_escape_string($connection, $id_value);
    $query = "SELECT * ";
    $query .= "FROM ".$safe_record;
    $query .= " WHERE ". $safe_id;
    $query .= " = '{$safe_id_value}' ";
    $query .= "LIMIT 1";
    $record_set = mysqli_query($connection, $query);
    confirm_query($record_set);
    if($record = mysqli_fetch_assoc($record_set)){
        return $record;
    } else {
        return null;
    }
}

function find_record_by_composite_key($record, $id1, $id2, $id_value1, $id_value2){
    global $connection;
    $safe_record = mysqli_real_escape_string($connection, $record);
    $safe_id1 = mysqli_real_escape_string($connection, $id1);
    $safe_id_value1 = mysqli_real_escape_string($connection, $id_value1);
    $safe_id2 = mysqli_real_escape_string($connection, $id2);
    $safe_id_value2 = mysqli_real_escape_string($connection, $id_value2);
    $query = "SELECT * ";
    $query .= "FROM ".$safe_record;
    $query .= " WHERE ". $safe_id1;
    $query .= " = '{$safe_id_value1}' ";
    $query .= " AND ". $safe_id2;
    $query .= " = '{$safe_id_value2}' ";
    $query .= "LIMIT 1";
    $record_set = mysqli_query($connection, $query);
    confirm_query($record_set);
    if($record = mysqli_fetch_assoc($record_set)){
        return $record;
    } else {
        return null;
    }
}

function find_records_by_filter($record, $filter, $filter_value, $order_by) {
    global $connection;
    global $company_id;
    $safe_record = mysqli_real_escape_string($connection, $record);
    $safe_filter = mysqli_real_escape_string($connection, $filter);
    $safe_filter_value = mysqli_real_escape_string($connection, $filter_value);
    $safe_order_by = mysqli_real_escape_string($connection, $order_by);
    $query = "SELECT * ";
    $query .= "FROM ". $safe_record;
    $query .= " WHERE ". $safe_filter;
    $query .= " = '{$safe_filter_value}' ";
    $query .= " ORDER BY " . $order_by . " ASC";
    $list_set = mysqli_query($connection, $query);
    confirm_query($list_set);
    return $list_set;
}

function find_record_by_filter_limit_one($record, $filter, $filter_value, $order_by){
    global $connection;
    global $company_id;
    $safe_record = mysqli_real_escape_string($connection, $record);
    $safe_filter = mysqli_real_escape_string($connection, $filter);
    $safe_filter_value = mysqli_real_escape_string($connection, $filter_value);
    $safe_order_by = mysqli_real_escape_string($connection, $order_by);
    $query = "SELECT * ";
    $query .= "FROM ". $safe_record;
    $query .= " WHERE ". $safe_filter;
    $query .= " = '{$safe_filter_value}' ";
    $query .= " ORDER BY " . $order_by . " ASC ";
    $query .= " LIMIT 1";
    $list_set = mysqli_query($connection, $query);
    confirm_query($list_set);
    return $list_set;
}

function find_records_by_two_filter($record, $filter, $filter2, $filter_value, $filter_value2, $order_by){
    global $connection;
    global $company_id;
    $safe_record = mysqli_real_escape_string($connection, $record);
    $safe_filter = mysqli_real_escape_string($connection, $filter);
    $safe_filter2 = mysqli_real_escape_string($connection, $filter2);
    $safe_filter_value = mysqli_real_escape_string($connection, $filter_value);
    $safe_filter_value2 = mysqli_real_escape_string($connection, $filter_value2);
    $safe_order_by = mysqli_real_escape_string($connection, $order_by);
    $query = "SELECT * ";
    $query .= "FROM ". $safe_record;
    $query .= " WHERE ". $safe_filter;
    $query .= " = '{$safe_filter_value}' ";
    $query .= " AND ". $safe_filter2;
    $query .= " = '{$safe_filter_value2}' ";
    $query .= " ORDER BY " . $order_by . " ASC";
    $list_set = mysqli_query($connection, $query);
    confirm_query($list_set);
    return $list_set;
}
