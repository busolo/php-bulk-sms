<?php

define("DB_SERVER", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "smspro");
// 1. Create a database connection
$connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

if(mysqli_connect_errno()){
    $_SESSION["message"] = "Database connection failed! ";
    die("Connection failed: " . mysqli_connect_error());
    //mysqli_connect_error() .
    //" ( " . mysqli_connect_errno() . ")";
}
