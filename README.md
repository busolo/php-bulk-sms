# PHP Bulk Sms
Php/MYSQL Bulk Sms System for sending SMS using different Service Providers; 

Neximo, Infobip and AfricasTalking with very minimal configuration. SMS are Queued using FIFO SplQueue before Sending Out

## Setup

Copy the Poject to your server

Configure mysql.class

Import DB

Configure API USER and KEY for each Service in tbl_service_providers

Set value **"1" in column "active"** for your desired Service Provider in tbl_service_providers. The default Service Provider is AfricasTalking

Happy SMSing

## Usage
### Web

Click on **Send All Pending Sms** Button on the Home Page
### Cron

You can send SMS by Cron Job
``` 00 00 * * * /usr/local/bin/php /home/user/SMSPro/SMSProcess/cronSMS.php ```

## Authors
*  [Samuel Masinde](https://bitbucket.org/%7B61279e2a-e528-42b0-9d2c-064276c5b4d0%7D/) 
