<?php


class Neximo implements SMSService {
    //Send SMS Via Neximo
    function sendSMS($phone, $msg) {
        // TODO: Implement sendSMS() method.
        $arrProvider = find_recordFilter("tbl_service_providers", "name", "AT", "name");
        $username   = $arrProvider['sp_username'];
        $apiKey     = $arrProvider['sp_key'];
        $basic  = new \Nexmo\Client\Credentials\Basic(NEXMO_API_KEY, NEXMO_API_SECRET);
        $client = new \Nexmo\Client($basic);
        try {
            $message = $client->message()->send([
                'to' => $phone,
                'from' => 'Acme Inc',
                'text' => $msg
            ]);
            $response = $message->getResponseData();
            if($response['messages'][0]['status'] == 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return true;
        }

    }
}