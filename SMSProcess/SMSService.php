<?php
	interface SMSService {
	    //Service that can be used by different Service Providers at run time
	    function sendSMS($phone, $msg);
	}