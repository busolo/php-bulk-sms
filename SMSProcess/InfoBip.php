<?php
class InfoBip implements SMSService{
//Send SMS through Infobip
    function sendSMS($phone, $sms) {
        // TODO: Implement sendSMS() method.
        $jsonData = array(
            'from' => 'InfoSMS',
            'to' => $phone,
            'text' => $sms
        );
        $payload = json_encode($jsonData);
        $arrProvider = find_recordFilter("tbl_service_providers", "name", "AT", "name");
        $username   = $arrProvider['sp_username'];
        if($username) {
            $url = "http://".$username."/sms/2/text/single";
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $payload,
                CURLOPT_HTTPHEADER => array(
                    "accept: application/json",
                    "authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==",
                    "content-type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
            if ($err) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}