<?php
include "../includes/loader.php";
include "AT.php";
include "InfoBip.php";
include "SmsQueue.php";
include "Neximo.php";
include "../model/Message.php";
class Sendsms {

    //Prepare SMS and Determine the Provider to be used
    public function processSms($phone, $msg, $sms_id) {
        $connection = mysqli_connect("localhost", "root", "", "smspro");
        //Get Service Provider
// Execute our query
        $arrProvider = find_recordFilter('tbl_service_providers', 'active', 1, 'active');
        //Get Configured Service Provider
        $ServiceType = $arrProvider['name'];
        if($ServiceType == 'Neximo'){
            $smsService = new Neximo();
        } else if($ServiceType == 'Infobip'){
            $smsService = new InfoBip();
        } else
            $smsService = new AT();
        if($smsService->sendSms($phone, $msg)) {
            $status = "SENT";
        } else {
            $status = "FAILED";
        }
        //UPDATE SMS Status with either FAILED or SENT
        $query = "UPDATE tbl_sms SET ";
        $query .="status = '{$status}' WHERE sms_id = '{$sms_id}' ";
        $query .="LIMIT 1";
        mysqli_query($connection, $query);
    }

    function sendTxt() {
        $sendBy = "AT";
        if($sendBy == 'AT'){
            $smsService = new AT();
        } else if($sendBy == 'InfoBip'){
            $smsService = new InfoBip();
        } else
            $smsService = new Neximo();
        $smsService->sendSMS("0720114007", "smsasSSA");
    }

    public function send() {
        $smsQueue = new SmsQueue();
        //Fetch Messages from the DB
        $queues = getQueue();
//loop through records and push to queue
        while($sms = mysqli_fetch_assoc($queues)) {
            if (strtotime($sms['datetime']) < strtotime('-2 day')) {
                $connection = mysqli_connect("localhost", "root", "", "smspro");
                //UPDATE SMS Status with either EXPIRED
                $expired = "EXPIRED";
                $query = "UPDATE tbl_sms SET ";
                $query .= "status = '{$expired}' WHERE sms_id = '{$sms['sms_id']}' ";
                $query .= "LIMIT 1";
                mysqli_query($connection, $query);
            } else {
                //Create an Instance of Message Object to store Values
                $message = new Message();
                $message->setSmsId($sms['sms_id']);
                $message->setPhoneNumber($sms['phone_number']);
                $message->setMessage($sms['message']);
                //Push Object into Queue
                $smsQueue->push($message);
            }
        }
//pop until nothing in  queue
        for($i = 0; $i < $smsQueue->count(); $i ++){
            $message = $smsQueue->pop();
            $this->processSms($message->getPhoneNumber(), $message->getMessage(), $message->getSmsId());
        }
    }
}