<?php
include "../library/AT/AfricasTalkingGateway.php";
include "SMSService.php";
//include("../includes/loader.php");
	class AT implements SMSService {
    //Send SMS Via AfricasTalking
		function sendSMS($phone, $msg) {
			// TODO: Implement sendSMS() method.
            $arrProvider = find_recordFilter("tbl_service_providers", "name", "AT", "name");
            $username   = $arrProvider['sp_username'];
            $apiKey     = $arrProvider['sp_key'];
            $gateway = new AfricasTalkingGateway($username, $apiKey);
            try {
                // Send a response originating from the short code that received the message
                return $gateway->sendMessage($phone, $msg);
            } catch ( AfricasTalkingGatewayException $e ) {
                // Log the error
                $errorMessage = $e->getMessage();
                echo $e->getMessage();
                return false;
            }
		}
	}
