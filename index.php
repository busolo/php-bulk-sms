<?php
    // Include necessary scripts and classes for entire project
    include("includes/loader.php");
    include("header.php");
?>
  <div class="container">
  <div class="row">
  <section id="main-content">
        <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-sm-4">
                <section>
                    <header class="panel-heading wht-bg">
                        <h4 class="gen-case text-left col-md-5"> Campaigns </h4>
                        <a href="#" class="btn btn-primary pull-right"data-toggle="modal" data-target="#addCampaign">Create Campaign</a>
                    </header>
                    <div class="panel-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Campaign Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                    $no_campaign = true;
                                    $campaigns = retrieve_list("tbl_campaigns", "campaign_id");
                                    while ($camps = mysqli_fetch_assoc($campaigns)) {
                                        $no_campaign = false; ?>
                                        <tr>
                                        <td><?= $camps['campaign_id'] ?></td>
                                        <td><?= $camps['name'] ?></td>
                                        <td><a href="<?php echo "composeSms.php?id=".$camps['campaign_id'] ?>">Send SMS</a></td>
                                        </tr>
                                <?php
                                    } ?>
                                </tbody>
                            </table>
                    </div>
                </section>
            </div>
            <div class="col-sm-8">
                <section>
                    <header class="panel-heading">
                       <h4 class="gen-case col-md-5"> List of SMS</h4>
                        <a href="#" id="sendAll" class="btn btn-success pull-right">Send All Pending Sms</a>
                    </header>
                    <?php if(!($no_campaign)) { ?>
                    <div class="panel-body">
                        <?php
                            $pendingSms = pendingSMS();
                            if (!(empty($pendingSms))) { ?>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Campaign</th>
                                <th>Datetime</th>
                                <th>Phone #</th>
                                <th>Message</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php while($campaigns = mysqli_fetch_assoc($pendingSms)) { ?>
                            <tr>
                                <td><?= $campaigns['sms_id'] ?></td>
                                <td><?= $campaigns['campaign'] ?></td>
                                <td><?= $campaigns['datetime'] ?></td>
                                <td><?= $campaigns['phone_number'] ?></td>
                                <td><?= $campaigns['message'] ?></td>
                                <td><?= $campaigns['status'] ?></td>
                            </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?php } else { ?>
                            <h4 class="text-primary">You have no SMS History, Click on "Compose SMS to create SMS</h4>
                            <a href="composeSms.php" class="btn btn-primary">Compose SMS</a>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </section>
            </div>
        </div>
        <!-- page end-->
        </section>
    </section>
  </div>
  </div>

    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
    <!-- Modal -->
    <div class="modal fade" id="addCampaign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Campaign</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="addCampaignForm" method="post" class="form-horizontal" enctype="multipart/form-data">
                        <label class="col-md-4">Campaign Name</label>
                        <div class="form-group col-md-8">
                            <div class="col-md-12">
                                <input type="text" tabindex="1" id="name" class="form-control" name="name">
                            </div>
                        </div>
                        <div class="compose-btn pull-right" style="margin:20px 0 !important">
                            <button type="submit" class="btn btn-primary"> Add Campaign </button>
                            <button class="btn" data-dismiss="modal"> Discard</button>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <script>
        // Bind to the submit event of our form
        $("#addCampaignForm").submit(function(e){
            // Prevent default posting of form
            e.preventDefault();
            var $form = $(this);
            // Serialize the data in the form
            var serializedData = $form.serialize();
            var phone = $("#phone").val();
            var type = $("#type").val();
            $.ajax({
                url: "addSMS.php",
                type: "post",
                data: {"name":$("#name").val(), "type": "campaign"},
                success: function(response){
                    if(!alert(response)){
                        window.location.replace("index.php");
                    }
                }
            });
        });
        // Bind to the submit event of our form
        $("#sendAll").click(function(e){
            // Prevent default posting of form
            e.preventDefault();
            var $form = $(this);
            $.ajax({
                url: "SMSProcess/cronSMS.php",
                type: "post",
                //data: {"name":$("#name").val(), "type": "campaign"},
                success: function(response){
                    if(!alert(response)){
                        window.location.replace("index.php");
                    }
                }
            });
        });
    </script>
<?php
    include("footer.php");
?>